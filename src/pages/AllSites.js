import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SiteList from '../components/Sites/SiteList';

import { getSitesData } from '../store/sites-slice';

const AllSites = () => {
  const dispatch = useDispatch();
  const sitesData = useSelector((state) => state.sites.data);

  useEffect(() => {
    dispatch(getSitesData());
  }, [dispatch]);

  return <SiteList sites={sitesData} />;
};

export default AllSites;
