import axios from 'axios';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import EditSite from './EditSite';
import store from '../store/index';
import { siteDetailsMock } from '../testing/SiteDetails.mock';
import userEvent from '@testing-library/user-event';

jest.mock('axios');
describe('Edit Site Component', () => {
  test('Renders the form with the data to edit when the request succeeds', async () => {
    axios.get.mockResolvedValueOnce({
      data: siteDetailsMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/edit-site/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/edit-site/:id" element={<EditSite />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const nameInput = await screen.findByLabelText('Nombre');
    expect(nameInput.value).toEqual(siteDetailsMock.name);
  });

  test('Renders the Error component when the request fails', async () => {
    axios.get.mockRejectedValueOnce({
      data: siteDetailsMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/sites/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/sites/:id" element={<EditSite />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const errorMsg = await screen.findByText(
      'Hubo un error al cargar los detalles para editar'
    );
    expect(errorMsg).toBeInTheDocument();
  });

  test('Renders the Error component when sending the edited data fails', async () => {
    axios.get.mockResolvedValueOnce({
      data: siteDetailsMock,
    });

    axios.put.mockRejectedValueOnce({
      data: siteDetailsMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/sites/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/sites/:id" element={<EditSite />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const submitButton = await screen.findByRole('button');
    const nameInput = await screen.findByLabelText('Nombre');
    const urlInput = await screen.findByLabelText('Url');
    userEvent.type(nameInput, 'Nuevo valor');
    userEvent.type(urlInput, 'Nueva Url');
    userEvent.click(submitButton);

    const errorMsg = await screen.findByText(
      'Hubo un error al enviar los datos.'
    );
    expect(errorMsg).toBeInTheDocument();
  });
});
