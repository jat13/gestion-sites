import AddSiteForm from '../components/Sites/AddSiteForm';

const NewSite = () => {
  return <AddSiteForm />;
};

export default NewSite;
