import axios from 'axios';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import Site from './Site';
import store from '../store/index';
import { siteDetailsMock } from '../testing/SiteDetails.mock';
import userEvent from '@testing-library/user-event';
import AllSites from './AllSites';
import { sitesListMock } from '../testing/SitesList.mock';

jest.mock('axios');
describe('Site Details Component', () => {
  test('Renders the Site details when the request succeeds', async () => {
    axios.get.mockResolvedValueOnce({
      data: siteDetailsMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/sites/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/sites/:id" element={<Site />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const nameValue = await screen.findByText(siteDetailsMock.name);
    expect(nameValue).toBeInTheDocument();
  });

  test('Renders the Error component when the request fails', async () => {
    axios.get.mockRejectedValueOnce({
      data: siteDetailsMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/sites/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/sites/:id" element={<Site />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const errorMsg = await screen.findByText(
      'Hubo un error al recuperar los detalles del sitio.'
    );
    expect(errorMsg).toBeInTheDocument();
  });

  test('Renders the home page with the Site list when delete succeeds', async () => {
    axios.get.mockResolvedValueOnce({
      data: siteDetailsMock,
    });

    axios.delete.mockResolvedValueOnce({
      data: siteDetailsMock._id,
    });

    axios.get.mockResolvedValueOnce({
      data: sitesListMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/sites/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/sites" element={<AllSites />} />
            <Route path="/sites/:id" element={<Site />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const deleteButton = await screen.findByRole('button', {
      name: 'Eliminar',
    });
    userEvent.click(deleteButton);
    const confirmButton = await screen.findByRole('button', {
      name: 'Eliminar',
    });
    userEvent.click(confirmButton);

    const detailsButtons = await screen.findAllByText('Ver detalles');
    expect(detailsButtons).toBeTruthy();
  });

  test('Renders the Error component when delete fails', async () => {
    axios.get.mockResolvedValueOnce({
      data: siteDetailsMock,
    });

    axios.delete.mockRejectedValueOnce({
      data: siteDetailsMock._id,
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`/sites/${siteDetailsMock._id}`]}>
          <Routes>
            <Route path="/sites/:id" element={<Site />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const deleteButton = await screen.findByRole('button', {
      name: 'Eliminar',
    });
    userEvent.click(deleteButton);
    const confirmButton = await screen.findByRole('button', {
      name: 'Eliminar',
    });
    userEvent.click(confirmButton);

    const errorMsg = await screen.findByText(
      'Hubo un error al eliminar el sitio.'
    );
    expect(errorMsg).toBeInTheDocument();
  });
});
