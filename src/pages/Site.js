import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getSiteDetails } from '../store/sites-slice';

import SiteDetails from '../components/Sites/SiteDetails';

const Site = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const siteDetails = useSelector((state) => state.sites.siteDetails);

  useEffect(() => {
    dispatch(getSiteDetails(params.id));
  }, [dispatch, params]);

  return <SiteDetails site={siteDetails} />;
};

export default Site;
