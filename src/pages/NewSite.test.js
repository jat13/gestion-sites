import axios from 'axios';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import store from '../store/index';
import { newSiteMock } from '../testing/NewSite.mock';
import NewSite from './NewSite';
import userEvent from '@testing-library/user-event';
import AllSites from './AllSites';
import { sitesListMock } from '../testing/SitesList.mock';
import { act } from 'react-dom/test-utils';

jest.mock('axios');
describe('New Site Component', () => {
  test('Renders the home page with the Site list when create succeeds', async () => {
    axios.post.mockResolvedValueOnce({
      data: newSiteMock,
    });

    axios.get.mockResolvedValueOnce({
      data: sitesListMock,
    });

    const dom = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/new-site']}>
          <Routes>
            <Route path="/sites" element={<AllSites />} />
            <Route path="/new-site" element={<NewSite />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const nameInput = await screen.findByLabelText('Nombre');
    userEvent.type(nameInput, newSiteMock.name);
    const pathInput = await screen.findByLabelText('Url');
    userEvent.type(pathInput, newSiteMock.path);
    const publicPathInput = await screen.findByLabelText('Url Pública');
    userEvent.type(publicPathInput, newSiteMock.publicPath);
    const descrInput = await screen.findByLabelText('Descripción');
    userEvent.type(descrInput, newSiteMock.description);
    const keyInput = await screen.findByLabelText('Key');
    userEvent.type(keyInput, newSiteMock.key);

    act(() => {
      const form = dom.container.querySelector('form');
      form.submit();
    });

    const detailsButtons = await screen.findAllByText('Ver detalles');
    expect(detailsButtons).toBeTruthy();
  });

  test('Renders the Error component when create fails', async () => {
    axios.post.mockRejectedValueOnce({
      data: newSiteMock,
    });

    const dom = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/new-site']}>
          <Routes>
            <Route path="/sites" element={<AllSites />} />
            <Route path="/new-site" element={<NewSite />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    const nameInput = await screen.findByLabelText('Nombre');
    userEvent.type(nameInput, newSiteMock.name);
    const pathInput = await screen.findByLabelText('Url');
    userEvent.type(pathInput, newSiteMock.path);
    const publicPathInput = await screen.findByLabelText('Url Pública');
    userEvent.type(publicPathInput, newSiteMock.publicPath);
    const descrInput = await screen.findByLabelText('Descripción');
    userEvent.type(descrInput, newSiteMock.description);
    const keyInput = await screen.findByLabelText('Key');
    userEvent.type(keyInput, newSiteMock.key);

    act(() => {
      const form = dom.container.querySelector('form');
      form.submit();
    });

    const errorMsg = await screen.findByText(
      'Hubo un error al enviar los datos.'
    );
    expect(errorMsg).toBeInTheDocument();
  });
});
