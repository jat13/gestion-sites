import axios from 'axios';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import AllSites from './AllSites';
import store from '../store/index';
import { sitesListMock } from '../testing/SitesList.mock';

jest.mock('axios');
describe('Sites List Component', () => {
  test('Renders the list of Sites when the request succeeds', async () => {
    axios.get.mockResolvedValueOnce({
      data: sitesListMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AllSites />
        </MemoryRouter>
      </Provider>
    );

    const sitesList = await screen.findAllByRole('listitem');
    expect(sitesList).not.toHaveLength(0);
  });

  test('Renders the Error component when the request fails', async () => {
    axios.get.mockRejectedValueOnce({
      data: sitesListMock,
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AllSites />
        </MemoryRouter>
      </Provider>
    );

    const errorMsg = await screen.findByText(
      'Hubo un error al recuperar los datos'
    );
    expect(errorMsg).toBeInTheDocument();
  });

  test('Renders empty list when the request succeeds but receives empty array', async () => {
    axios.get.mockResolvedValueOnce({
      data: [],
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AllSites />
        </MemoryRouter>
      </Provider>
    );

    const sitesList = await screen.findByRole('list');
    expect(sitesList.children).toHaveLength(0);
  });
});
