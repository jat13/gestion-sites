import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getSiteDetails } from '../store/sites-slice';
import Loading from '../components/ui/Loading';

import EditSiteForm from '../components/Sites/EditSiteForm';
import Error from '../components/ui/Error';

const EditSite = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const siteDetails = useSelector((state) => state.sites.siteDetails);
  const isLoading = useSelector((state) => state.ui.isLoading);
  const errorRetrieving = useSelector((state) => state.ui.hasErrors);

  useEffect(() => {
    dispatch(getSiteDetails(params.id));
  }, [dispatch, params]);

  const onRetryHandler = () => dispatch(getSiteDetails(params.id));

  return (
    <>
      {isLoading && <Loading />}
      {errorRetrieving && (
        <Error retry={onRetryHandler}>
          Hubo un error al cargar los detalles para editar
        </Error>
      )}
      {!isLoading && !errorRetrieving && (
        <EditSiteForm siteData={siteDetails} />
      )}
    </>
  );
};

export default EditSite;
