import { useFormik } from 'formik';

const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Requerido';
  }
  if (!values.path) {
    errors.path = 'Requerido';
  }
  if (!values.publicPath) {
    errors.publicPath = 'Requerido';
  }
  if (!values.description) {
    errors.description = 'Requerido';
  }
  if (!values.key) {
    errors.key = 'Requerido';
  }

  return errors;
};

const BuildForm = (initialValue, submitHandler) => {
  let value = {};
  if (Object.entries(initialValue).length === 0) {
    value = {
      name: '',
      path: '',
      publicPath: '',
      description: '',
      key: '',
    };
  } else {
    value = { ...initialValue };
  }

  const formik = useFormik({
    initialValues: { ...value },
    validate,
    onSubmit: (values) => {
      submitHandler(values);
    },
  });

  return formik;
};

export default BuildForm;
