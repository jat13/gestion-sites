import { Route, Routes, Navigate } from 'react-router-dom';
import Layout from './components/ui/Layout';
import AllSites from './pages/AllSites';
import Site from './pages/Site';
import NewSite from './pages/NewSite';
import EditSite from './pages/EditSite';
import CustomSuccessSnackbar from './components/ui/CustomSuccessSnackbar';

function App() {
  return (
    <>
      <Layout>
        <Routes>
          <Route path="/sites" element={<AllSites />} />
          <Route path="/sites/:id" element={<Site />} />
          <Route path="/new-site" element={<NewSite />} />
          <Route path="/edit-site/:id" element={<EditSite />} />
          <Route path="/" element={<Navigate replace to="/sites" />} />
        </Routes>
        <CustomSuccessSnackbar />
      </Layout>
    </>
  );
}

export default App;
