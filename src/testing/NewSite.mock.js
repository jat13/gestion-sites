export const newSiteMock = {
  name: 'Nuevo site',
  path: 'newPath',
  publicPath: 'newPublicPath',
  key: 'key',
  description: 'description',
};
