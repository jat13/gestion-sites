export const siteDetailsMock = {
  _id: '62fa8ff2be4de5f0499fffc6',
  name: 'Neox',
  path: 'neoxPath',
  publicPath: 'neoxPublicPath',
  key: 'key',
  description: 'description',
  site: '1660630103435',
  createDate: '2022-08-16T06:08:23.432Z',
  __v: 0,
};
