import { useSelector, useDispatch } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import Error from '../ui/Error';
import Loading from '../ui/Loading';
import Card from '../ui/Card';
import Button from '../ui/Button';

import { deleteSite, getSiteDetails } from '../../store/sites-slice';

import styles from './SiteDetails.module.css';
import { useEffect, useState } from 'react';
import CustomDialog from '../ui/CustomDialog';
import { uiActions } from '../../store/ui-slice';

const SiteDetails = (props) => {
  const [openDialog, setOpenDialog] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const params = useParams();
  const isLoading = useSelector((state) => state.ui.isLoading);
  const hasErrors = useSelector((state) => state.ui.hasErrors);
  const hasDeleted = useSelector((state) => state.ui.finishedSubmitting);
  const hasErrorsDeleting = useSelector((state) => state.ui.hasErrorsDeleting);

  useEffect(() => {
    if (hasDeleted) {
      navigate('/sites');
      dispatch(uiActions.setFinishedSubmitting(false));
    }
  }, [hasDeleted, navigate, dispatch]);

  const onRetryHandler = () => {
    dispatch(getSiteDetails(params.id));
  };

  let date = '';
  if (props.site.createDate) {
    date = new Date(props.site.createDate);
    date = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  }

  const onBackHandler = () => {
    navigate('/sites');
  };

  const onDeleteHandler = () => {
    setOpenDialog(true);
  };

  const onDeleteCancelHandler = () => {
    setOpenDialog(false);
  };

  const onDeleteConfirmHandler = () => {
    setOpenDialog(false);
    dispatch(deleteSite(params.id));
  };

  const onEditHandler = () => {
    navigate(`/edit-site/${params.id}`);
  };

  return (
    <>
      {isLoading && <Loading />}
      {hasErrors && (
        <Error retry={onRetryHandler}>
          Hubo un error al recuperar los detalles del sitio.
        </Error>
      )}
      {hasErrorsDeleting && (
        <Error retry={onDeleteHandler}>
          Hubo un error al eliminar el sitio.
        </Error>
      )}
      {!isLoading && !hasErrors && !hasErrorsDeleting && (
        <>
          <Card>
            <div className={styles.details}>
              <h1 className={styles.title}>{props.site.name}</h1>
              <h1 className={styles.description}>{props.site.description}</h1>
              <span className={styles.path}>
                <p>
                  <b>Url privada: </b>
                  <a href={props.site.path}>{props.site.path}</a>
                </p>
              </span>
              <span className={styles.path}>
                <p>
                  <b>Url pública: </b>
                  <a href={props.site.publicPath}>{props.site.publicPath}</a>
                </p>
              </span>
              <p>
                <b>Key: </b>
                {props.site.key}
              </p>
              <p>
                <b>Site: </b>
                {props.site.site}
              </p>
              <p>
                <b>Fecha de creación: </b>
                {date}
              </p>
              <div className={styles.actions}>
                <Button onClick={onBackHandler}>Volver</Button>
                <Button onClick={onEditHandler}>Editar</Button>
                <Button onClick={onDeleteHandler}>Eliminar</Button>
              </div>
            </div>
          </Card>
        </>
      )}
      <CustomDialog
        open={openDialog}
        title="¿Está seguro que desea eliminar los datos?"
        onCancelDelete={onDeleteCancelHandler}
        onConfirmDelete={onDeleteConfirmHandler}
      />
    </>
  );
};

export default SiteDetails;
