import { Link } from 'react-router-dom';
import Button from '../ui/Button';
import Card from '../ui/Card';

import styles from './ListItem.module.css';

const ListItem = (props) => {
  return (
    <li>
      <Card>
        <h1>{props.name}</h1>
        <p className={styles.description}>{props.description}</p>
        <p className={styles.path}>
          <a href={props.path}>{props.path}</a>
        </p>
        <p className={styles.path}>
          <a href={props.publicPath}>{props.publicPath}</a>
        </p>
        <div className={styles.actions}>
          <Link to={`/sites/${props.id}`}>
            <Button>Ver detalles</Button>
          </Link>
        </div>
      </Card>
    </li>
  );
};

export default ListItem;
