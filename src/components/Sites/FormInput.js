import styles from './FormInput.module.css';

const FormInput = (props) => {
  return (
    <>
      <label htmlFor={props.name}>{props.label}</label>
      <input
        className={
          props.form.errors[`${props.name}`] &&
          props.form.touched[`${props.name}`]
            ? styles.required
            : ''
        }
        id={props.name}
        name={props.name}
        type={props.type}
        onChange={props.form.handleChange}
        onBlur={props.form.handleBlur}
        value={props.form.values[`${props.name}`]}
      />
    </>
  );
};

export default FormInput;
