import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import BuildForm from '../../utils/forms/SitesForm';
import Button from '../ui/Button';
import Card from '../ui/Card';
import FormInput from './FormInput';
import Error from '../ui/Error';

import styles from './EditSiteForm.module.css';
import { editSite } from '../../store/sites-slice';
import { uiActions } from '../../store/ui-slice';
import { useEffect } from 'react';

const EditSiteForm = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const hasErrorsEditing = useSelector((state) => state.ui.hasErrorsEditing);
  const hasEdited = useSelector((state) => state.ui.finishedEditing);

  useEffect(() => {
    if (hasEdited) {
      navigate(`/sites/${props.siteData._id}`);
      dispatch(uiActions.setFinishedEditing(false));
    }
  }, [hasEdited, navigate, dispatch, props.siteData._id]);

  const onSubmitHandler = (site) => {
    dispatch(editSite(props.siteData._id, site));
  };

  const initialValue = {
    name: props.siteData.name,
    path: props.siteData.path,
    publicPath: props.siteData.publicPath,
    description: props.siteData.description,
    key: props.siteData.key,
  };

  const editForm = BuildForm(initialValue, onSubmitHandler);

  const onRetryHandler = () => {
    navigate(`/edit-site/${props.siteData._id}`);
  };

  const onEditCancel = () => {
    navigate(`/sites/${props.siteData._id}`);
  };

  return (
    <>
      {hasErrorsEditing && (
        <Error retry={onRetryHandler}>Hubo un error al enviar los datos.</Error>
      )}
      {!hasErrorsEditing && (
        <Card>
          <form className={styles.form} onSubmit={editForm.handleSubmit}>
            <FormInput form={editForm} name="name" type="text" label="Nombre" />
            <FormInput form={editForm} name="path" type="text" label="Url" />
            <FormInput
              form={editForm}
              name="publicPath"
              type="text"
              label="Url Pública"
            />
            <FormInput
              form={editForm}
              name="description"
              type="text"
              label="Descripción"
            />
            <FormInput form={editForm} name="key" type="text" label="Key" />
            <Button onClick={onEditCancel}>Cancelar</Button>
            <Button
              submit
              disabled={
                !editForm.isValid || Object.keys(editForm.touched).length === 0
              }
            >
              Enviar
            </Button>
          </form>
        </Card>
      )}
    </>
  );
};

export default EditSiteForm;
