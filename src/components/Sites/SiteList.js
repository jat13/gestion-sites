import { useSelector, useDispatch } from 'react-redux';
import Error from '../ui/Error';
import Loading from '../ui/Loading';
import ListItem from './ListItem';

import { getSitesData } from '../../store/sites-slice';

import styles from './SiteList.module.css';

const SiteList = (props) => {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.ui.isLoading);
  const hasErrors = useSelector((state) => state.ui.hasErrors);

  const onRetryHandler = () => {
    dispatch(getSitesData());
  };

  return (
    <>
      {isLoading && <Loading />}
      {hasErrors && (
        <Error retry={onRetryHandler}>
          Hubo un error al recuperar los datos
        </Error>
      )}
      {!isLoading && !hasErrors && (
        <ul className={styles.list}>
          {props.sites.map((site) => (
            <ListItem
              key={site._id}
              id={site._id}
              name={site.name}
              path={site.path}
              publicPath={site.publicPath}
              description={site.description}
            />
          ))}
        </ul>
      )}
    </>
  );
};

export default SiteList;
