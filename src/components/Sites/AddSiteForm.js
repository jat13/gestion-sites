import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import BuildForm from '../../utils/forms/SitesForm';
import Button from '../ui/Button';
import Card from '../ui/Card';
import FormInput from './FormInput';
import { createSite } from '../../store/sites-slice';

import styles from './AddSiteForm.module.css';
import Loading from '../ui/Loading';
import Error from '../ui/Error';
import { useEffect } from 'react';
import { uiActions } from '../../store/ui-slice';

const AddSiteForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isLoading = useSelector((state) => state.ui.isLoading);
  const hasErrors = useSelector((state) => state.ui.hasErrors);
  const hasFinished = useSelector((state) => state.ui.finishedSubmitting);

  useEffect(() => {
    if (hasFinished) {
      navigate('/sites');
      dispatch(uiActions.setFinishedSubmitting(false));
    }
  }, [hasFinished, navigate, dispatch]);

  const onSubmitHandler = (site) => {
    dispatch(createSite(site));
  };

  const addForm = BuildForm({}, onSubmitHandler);

  const onRetryHandler = () => {
    dispatch(createSite(addForm.values));
  };

  const onCreateCancel = () => {
    navigate('/sites');
  };

  return (
    <>
      {isLoading && <Loading />}
      {hasErrors && (
        <Error retry={onRetryHandler}>Hubo un error al enviar los datos.</Error>
      )}
      {!isLoading && !hasErrors && (
        <Card>
          <form className={styles.form} onSubmit={addForm.handleSubmit}>
            <FormInput form={addForm} name="name" type="text" label="Nombre" />
            <FormInput form={addForm} name="path" type="text" label="Url" />
            <FormInput
              form={addForm}
              name="publicPath"
              type="text"
              label="Url Pública"
            />
            <FormInput
              form={addForm}
              name="description"
              type="text"
              label="Descripción"
            />
            <FormInput form={addForm} name="key" type="text" label="Key" />
            <Button onClick={onCreateCancel}>Cancelar</Button>
            <Button
              submit
              disabled={
                !addForm.isValid || Object.keys(addForm.touched).length === 0
              }
            >
              Enviar
            </Button>
          </form>
        </Card>
      )}
    </>
  );
};

export default AddSiteForm;
