import { Snackbar, Alert } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { uiActions } from '../../store/ui-slice';

const CustomSuccessSnackbar = () => {
  const dispatch = useDispatch();
  const snackbarOpen = useSelector((state) => state.ui.snackbarOpen);
  const snackbarMessage = useSelector((state) => state.ui.snackbarMessage);

  const handleClose = () => {
    dispatch(uiActions.setSnackbarOpen(false));
  };

  return (
    <Snackbar open={snackbarOpen} autoHideDuration={5000} onClose={handleClose}>
      <Alert severity="success" sx={{ width: '100%' }}>
        {snackbarMessage}
      </Alert>
    </Snackbar>
  );
};

export default CustomSuccessSnackbar;
