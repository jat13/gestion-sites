import { Link } from 'react-router-dom';
import Button from './Button';

import styles from './Header.module.css';

const Header = () => {
  return (
    <header className={styles.header}>
      <Link to="/" className={styles.link}>
        <div className={styles.title}>Gestión de Sites</div>
      </Link>
      <div className={styles.actions}>
        <Link to="/new-site">
          <Button header="header">Añadir Site</Button>
        </Link>
      </div>
    </header>
  );
};

export default Header;
