import styles from './Button.module.css';

const Button = (props) => {
  return (
    <button
      className={props.header ? `${styles.buttonHeader}` : `${styles.button}`}
      type={props.submit ? 'submit' : 'button'}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.children}
    </button>
  );
};

export default Button;
