import Button from './Button';
import Card from './Card';

import styles from './Error.module.css';

const Error = (props) => {
  return (
    <Card>
      <div className={styles.content}>
        <p>{props.children}</p>
        <Button onClick={props.retry}>Reintentar</Button>
      </div>
    </Card>
  );
};
export default Error;
