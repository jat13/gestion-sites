import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import uiSlice from './ui-slice';

const API_URL = process.env.REACT_APP_API_URL;

const initialSitesState = {
  data: [],
  siteDetails: {},
};

const sitesSlice = createSlice({
  name: 'sites',
  initialState: initialSitesState,
  reducers: {
    setSitesData(state, action) {
      state.data = action.payload;
    },
    setSiteDetails(state, action) {
      state.siteDetails = action.payload;
    },
  },
});

export const getSitesData = () => {
  return async (dispatch) => {
    dispatch(uiSlice.actions.setInitialStates());

    const fetchData = async () => {
      const response = await axios
        .get(`${API_URL}/sites`)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error);
        });

      return response;
    };

    try {
      const response = await fetchData();
      dispatch(sitesSlice.actions.setSitesData(response));
    } catch (error) {
      dispatch(uiSlice.actions.setErrors(true));
    }

    dispatch(uiSlice.actions.setLoading(false));
  };
};

export const getSiteDetails = (id) => {
  return async (dispatch) => {
    dispatch(uiSlice.actions.setInitialStates());

    const fetchData = async () => {
      const response = await axios
        .get(`${API_URL}/site/${id}`)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error);
        });

      return response;
    };

    try {
      const response = await fetchData();
      dispatch(sitesSlice.actions.setSiteDetails(response));
    } catch (error) {
      dispatch(uiSlice.actions.setErrors(true));
    }
    dispatch(uiSlice.actions.setLoading(false));
  };
};

export const createSite = (siteData) => {
  return async (dispatch) => {
    dispatch(uiSlice.actions.setInitialStates());

    const sendData = async () => {
      const response = await axios
        .post(`${API_URL}/site`, siteData)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error);
        });

      return response;
    };

    try {
      await sendData();
      dispatch(uiSlice.actions.setFinishedSubmitting(true));
      dispatch(uiSlice.actions.setSnackbarOpen(true));
      dispatch(uiSlice.actions.setSnackbarMessage('Site creado correctamente'));
    } catch (error) {
      dispatch(uiSlice.actions.setErrors(true));
    }
    dispatch(uiSlice.actions.setLoading(false));
  };
};

export const deleteSite = (id) => {
  return async (dispatch) => {
    dispatch(uiSlice.actions.setInitialStates());

    const deleteData = async () => {
      const response = await axios
        .delete(`${API_URL}/site/${id}`)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error);
        });

      return response;
    };

    try {
      await deleteData();
      dispatch(uiSlice.actions.setFinishedSubmitting(true));
      dispatch(uiSlice.actions.setSnackbarOpen(true));
      dispatch(
        uiSlice.actions.setSnackbarMessage('Site eliminado correctamente')
      );
    } catch (error) {
      dispatch(uiSlice.actions.setHasErrorsDeleting(true));
    }
    dispatch(uiSlice.actions.setLoading(false));
  };
};

export const editSite = (id, site) => {
  return async (dispatch) => {
    dispatch(uiSlice.actions.setInitialStates());

    const sendData = async () => {
      const response = await axios
        .put(`${API_URL}/site/${id}`, site)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          throw new Error(error);
        });

      return response;
    };

    try {
      await sendData();
      dispatch(uiSlice.actions.setFinishedEditing(true));
      dispatch(uiSlice.actions.setSnackbarOpen(true));
      dispatch(
        uiSlice.actions.setSnackbarMessage('Site editado correctamente')
      );
    } catch (error) {
      dispatch(uiSlice.actions.setHasErrorsEditing(true));
    }
    dispatch(uiSlice.actions.setLoading(false));
  };
};

export const sitesActions = sitesSlice.actions;

export default sitesSlice;
