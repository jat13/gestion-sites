import { createSlice } from '@reduxjs/toolkit';

const initialUiState = {
  isLoading: false,
  hasErrors: false,
  finishedSubmitting: false,
  finishedEditing: false,
  hasErrorsDeleting: false,
  hasErrorsEditing: false,
  snackbarOpen: false,
  snackbarMessage: '',
};

const uiSlice = createSlice({
  name: 'ui',
  initialState: initialUiState,
  reducers: {
    setLoading(state, action) {
      state.isLoading = action.payload;
    },
    setErrors(state, action) {
      state.hasErrors = action.payload;
    },
    setFinishedSubmitting(state, action) {
      state.finishedSubmitting = action.payload;
    },
    setFinishedEditing(state, action) {
      state.finishedEditing = action.payload;
    },
    setHasErrorsDeleting(state, action) {
      state.hasErrorsDeleting = action.payload;
    },
    setHasErrorsEditing(state, action) {
      state.hasErrorsEditing = action.payload;
    },
    setSnackbarOpen(state, action) {
      state.snackbarOpen = action.payload;
    },
    setSnackbarMessage(state, action) {
      state.snackbarMessage = action.payload;
    },
    setInitialStates(state, action) {
      state.hasErrors = false;
      state.hasErrorsDeleting = false;
      state.hasErrorsEditing = false;
      state.isLoading = true;
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice;
