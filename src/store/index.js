import { configureStore } from '@reduxjs/toolkit';
import sitesSlice from './sites-slice';
import uiSlice from './ui-slice';

const store = configureStore({
  reducer: { sites: sitesSlice.reducer, ui: uiSlice.reducer },
});

export default store;
